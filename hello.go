package main // 是一个特殊的package ， main 函数是一个特殊的函数，是一个程序的入口

import "fmt" // 输入输出、格式化等

func main() {
	fmt.Println("Hello, GO !")
}
